"""
Submit a transaction batch read from file to sawtooth network.
"""


def submit_transaction(binary_data):
    import urllib.request
    from urllib.error import HTTPError
    print('Sending...\n', binary_data, "\n number of bytes ", len(binary_data))
    try:
        request = urllib.request.Request(
            'http://localhost:8080/batches',
            binary_data,
            method='POST',
            headers={'Content-Type': 'application/octet-stream'})
        response = urllib.request.urlopen(request)
        print(response.getcode(), "\n", response.read())
    except HTTPError as e:
        response = e.file
        print("Error:\n", response.getcode(), "\n", response.read())


dataFile = 'last_transaction_java.bin'

# keyfile = 'myfile.bin'

try:
    with open(dataFile, 'rb') as fd:
        data_bytes = fd.read()
        fd.close()
except OSError as err:
    raise Exception(
        'Failed to read file {}'.format(str(err)))

submit_transaction(data_bytes)



