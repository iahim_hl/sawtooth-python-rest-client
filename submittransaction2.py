""" A sample application for submitting transaction on Sawtooth network based on
the instructions from here: https://sawtooth.hyperledger.org/docs/core/releases/latest/_autogen/txn_submit_tutorial.html#creating-private-and-public-keys

- if key is not provided it is generated and stored in a last_key.priv file
- before submitting transaction, the binary data containing the submitted transaction is saved to last_transaction.bin file

"""

import secp256k1
from hashlib import sha512
import hashlib
import sys


# util functions
from sawtooth_signing import CryptoFactory, create_context
from sawtooth_signing.secp256k1 import Secp256k1PrivateKey


def _sha512(data):
    return hashlib.sha512(data).hexdigest()


def _get_prefix():
    return _sha512('intkey'.encode('utf-8'))[0:6]


def _get_address(name):
    prefix = _get_prefix()
    game_address = _sha512(name.encode('utf-8'))[64:]
    return prefix + game_address


def load_from_file(keyfile):
    if keyfile is not None:
        try:
            with open(keyfile,'rb') as fd:
                private_key_bytes = fd.read().strip()
                fd.close()
        except OSError as err:
            raise Exception(
                'Failed to read private key: {}'.format(str(err)))
        _key_handler = secp256k1.PrivateKey(private_key_bytes)
        return _key_handler

# generate private and public keys
if len(sys.argv) == 1:
    print('Generate private key.')
    key_handler = secp256k1.PrivateKey()
else:
    print('Load private key from file.')
    key_handler = load_from_file(sys.argv[1])

private_key_bytes = key_handler.private_key
public_key_bytes = key_handler.pubkey.serialize()
public_key_hex = public_key_bytes.hex()

# save key
key_file=open('last_key.priv','wb')
key_file.write(private_key_bytes)
key_file.close()

# create payload

import cbor

payload = {
    'Verb': 'set',
    'Name': '999',
    'Value': 42}

payload_bytes = cbor.dumps(payload)

payload_sha512 = sha512(payload_bytes).hexdigest()

from random import randint
from sawtooth_sdk.protobuf.transaction_pb2 import TransactionHeader

address = _get_address('999')

txn_header = TransactionHeader(
    batcher_pubkey=public_key_hex,
    # If we had any dependencies, this is what it might look like:
    # dependencies=['540a6803971d1880ec73a96cb97815a95d374cbad5d865925e5aa0432fcf1931539afe10310c122c5eaae15df61236079abbf4f258889359c4d175516934484a'],
    family_name='intkey',
    family_version='1.0',
    inputs=[address],
    nonce=str(randint(0, 1000000000)),
    outputs=[address],
    payload_encoding='application/cbor',
    payload_sha512=payload_sha512,
    signer_pubkey=public_key_hex)

txn_header_bytes = txn_header.SerializeToString()

key_handler = secp256k1.PrivateKey(private_key_bytes)

# ecdsa_sign automatically generates a SHA-256 hash of the header bytes
txn_signature = key_handler.ecdsa_sign(txn_header_bytes)
txn_signature_bytes = key_handler.ecdsa_serialize_compact(txn_signature)
txn_signature_hex = txn_signature_bytes.hex()

from sawtooth_sdk.protobuf.transaction_pb2 import Transaction

txn = Transaction(
    header=txn_header_bytes,
    header_signature=txn_signature_hex,
    payload=payload_bytes)

from sawtooth_sdk.protobuf.batch_pb2 import BatchHeader

batch_header = BatchHeader(
    signer_pubkey=public_key_hex,
    transaction_ids=[txn.header_signature])

batch_header_bytes = batch_header.SerializeToString()

# dign the headers

batch_signature = key_handler.ecdsa_sign(batch_header_bytes)

batch_signature_bytes = key_handler.ecdsa_serialize_compact(batch_signature)

batch_signature_hex = batch_signature_bytes.hex()

# create the batch
from sawtooth_sdk.protobuf.batch_pb2 import Batch

batch = Batch(
    header=batch_header_bytes,
    header_signature=batch_signature_hex,
    transactions=[txn])

# encode batch
from sawtooth_sdk.protobuf.batch_pb2 import BatchList

batch_list = BatchList(batches=[batch])
batch_bytes = batch_list.SerializeToString()

output_file = open("last_transaction.bin", "wb")
output_file.write(batch_bytes)
output_file.close()


# submit batch to validator
def submit_transaction(binary_data):
    import urllib.request
    from urllib.error import HTTPError
    print('Sending...\n', binary_data, "\n number of bytes ", len(binary_data))
    try:
        request = urllib.request.Request(
            'http://localhost:8080/batches',
            batch_bytes,
            method='POST',
            headers={'Content-Type': 'application/octet-stream'})
        response = urllib.request.urlopen(request)
        print(response.read())
    except HTTPError as e:
        response = e.file


submit_transaction(batch_bytes)



